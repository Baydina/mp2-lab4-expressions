#include "gtest.h"

#include "expressions.h"

TEST(Transfer, translation_of_simple_expression)
{
	EXPECT_EQ(Transfer("2*(8-5)+4"),"2 8 5-* 4+");
}

TEST(Transfer, translating_a_simple_expression_of_the_real)
{
	EXPECT_EQ(Transfer("(1+2)/(3+4*6.7)-5.3*4.4"),"1 2+ 3 4 6.7*+/ 5.3 4.4*-");
}


TEST(Transfer, translating_many_digit_number)
{
	EXPECT_EQ(Transfer("100+20000/3500*(1000+21000000)"),"100 20000 3500/ 1000 21000000+*+");
}

TEST(Transfer, translation_expression_with_real_lot_of_decimal_places)
{
	EXPECT_EQ(Transfer("(0.0000032+0.0000068)*100"), "0.0000032 0.0000068+ 100*");
}

TEST(Transfer, not_correct_placement_of_parentheses)
{
	ASSERT_ANY_THROW(Transfer("2*(8-5)+4)"));
}

TEST(Transfer, correct_placement_of_parentheses)
{
	ASSERT_NO_THROW(Transfer("2*(8-5)+4"));
}

TEST(Computation, computation_of_simple_expression)
{
	float res=2*(8-5)+4;
	EXPECT_FLOAT_EQ(Computation("2*(8-5)+4"),res);
}

TEST(Computation, computation_a_simple_expression_of_the_real)
{
	float res=(1+2)/(3+4*6.7)-5.3*4.4;
	EXPECT_FLOAT_EQ(Computation("(1+2)/(3+4*6.7)-5.3*4.4"),res);
}

TEST(Computation, computation_many_digit_number)
{
	float res=100+20000.f/3500*(1000+21000000);
	EXPECT_FLOAT_EQ(Computation("100+20000/3500*(1000+21000000)"),res);
}

TEST(Computation, computation_expression_with_real_lot_of_decimal_places)
{
	float res=(0.0000032+0.0000068)*100;
	EXPECT_FLOAT_EQ(Computation("(0.0000032+0.0000068)*100"), res);
}

TEST(Computation, not_correct_placement_of_parentheses)
{
	ASSERT_ANY_THROW(Computation("(1+2)(/(3+4*6.7)-5.3*4.4"));
}

TEST(Computation, correct_placement_of_parentheses)
{
	ASSERT_NO_THROW(Computation("(1+2)/(3+4*6.7)-5.3*4.4"));
}
